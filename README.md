# darnet

More details: http://pjreddie.com/darknet/yolo/


1.  Repository supports:
- Both Windows and Linux
- Both OpenCV 2.x.x and OpenCV <= 3.4.0 (3.4.1 and higher isn't supported, but you can try)
- Both cuDNN >= v7
- CUDA >= 7.5


2.  Requirements:
- Linux GCC>=4.9 or Windows MS Visual Studio 2015 (v140): https://go.microsoft.com/fwlink/?LinkId=532606&clcid=0x409 (or offline ISO image)
- CUDA 10.0: https://developer.nvidia.com/cuda-toolkit-archive (on Linux do Post-installation Actions)
- OpenCV 3.3.0: https://sourceforge.net/projects/opencvlibrary/files/opencv-win/3.3.0/opencv-3.3.0-vc14.exe/download
- GPU with CC >= 3.0: https://en.wikipedia.org/wiki/CUDA#GPUs_supported
--
